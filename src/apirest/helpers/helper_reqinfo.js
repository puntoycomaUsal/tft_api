'use strict';
const winston = require('winston')

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'workers.log' })
  ]
});

const loggerRequest = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: 'request.log' })
  ]
});

/**
 * Función que muestra la información de traza en el servidor al recibir
 * una petición request
 * @param  {object} req Objeto request con la información de la solicitud-petición HTTP
 */
exports.maininfo = (req) => {

    loggerRequest.info(``);
    loggerRequest.info(`Solicitud entrante metodo: ${req.method} originalURL: ${req.originalUrl}`);
    loggerRequest.info `parameters: ${req.parameters}`;
    loggerRequest.info `headers: ${req.headers}`;
    loggerRequest.info `body: ${req.body}`;

}

/**
 * Funcion que 
 * @param  {object} req Objeto request con la información de la solicitud-petición HTTP
 */
exports.workersControl = (message) => {
    logger.info(message);
}