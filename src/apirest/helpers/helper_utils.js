'use strict';
/**
 * Función que recibe el resultado que arroja una consulta de tipo
 * UPDATE, DELETE y genera en consecuencia un código y mensaje de estado
 * @param  {object} resultMysql Objeto con información sobre la consulta devuelto por la función query
 * @return {object}             Objeto que contiene el mensaje de estado y el código
 */
exports.handleMysqlResult = (resultMysql) => {

    let message = resultMysql.affectedRows ? "OK" : "Not found";
    let status  = resultMysql.affectedRows ? 200 : 404;

    return {message: message, code: status};

}