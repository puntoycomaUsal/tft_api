'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getAlbum = (albumId, callback) => {

    var query = "SELECT * FROM albums WHERE id= ?";
    var value = albumId;
    mysql_db.connection.query(query, value, callback);

}

exports.listAlbum = (callback) => {
    var query = "SELECT * FROM albums";
    mysql_db.connection.query(query, callback);
}

exports.createAlbum = (album, callback) => {

    var query = "INSERT INTO albums SET ?";
    var value = album
    mysql_db.connection.query(query, value, callback);

}

exports.deleteAlbum = (albumId, callback) => {

    var query = "DELETE FROM albums WHERE id = ?";
    var value = albumId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateAlbum = (albumId, newAlbum, callback) => {

    var query = "UPDATE albums SET ? WHERE id = ?";
    var value = [newAlbum, albumId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemsForAlbum = (albumId, callback) => {
    var query = "SELECT * FROM items WHERE id IN (SELECT item_id FROM items_albums WHERE album_id = ?)";
    var value = [albumId];
    mysql_db.connection.query(query, value, callback);

}

exports.getTagsForAlbum = (albumId, callback) => {
    var query = "SELECT * FROM tags WHERE id IN (SELECT tag_id FROM tags_albums WHERE album_id = ?)";
    var value = [albumId];
    mysql_db.connection.query(query, value, callback);

}