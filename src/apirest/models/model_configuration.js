'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getConfiguration = (configurationId, callback) => {

    var query = "SELECT * FROM configurations WHERE id= ?";
    var value = configurationId;
    mysql_db.connection.query(query, value, callback);

}

exports.listConfiguration = (callback) => {
    var query = "SELECT * FROM configurations";
    mysql_db.connection.query(query, callback);
}

exports.createConfiguration = (configuration, callback) => {

    var query = "INSERT INTO configurations SET ?";
    var value = configuration
    mysql_db.connection.query(query, value, callback);

}

exports.deleteConfiguration = (configurationId, callback) => {

    var query = "DELETE FROM configurations WHERE id = ?";
    var value = configurationId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateConfiguration = (configurationId, newConfiguration, callback) => {

    var query = "UPDATE configurations SET ? WHERE id = ?";;
    var value = [newConfiguration, configurationId];
    mysql_db.connection.query(query, value, callback);

}