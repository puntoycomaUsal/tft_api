'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getBandArtist = (bandId, callback) => {

    var query = "SELECT * FROM bands_artists WHERE id= ?";
    var value = bandId;
    mysql_db.connection.query(query, value, callback);

}

exports.listBandArtist = (callback) => {
    var query = "SELECT * FROM bands_artists";
    mysql_db.connection.query(query, callback);
}

exports.createBandArtist = (band, callback) => {

    var query = "INSERT INTO bands_artists SET ?";
    var value = band
    mysql_db.connection.query(query, value, callback);

}

exports.deleteBandArtist = (bandId, callback) => {

    var query = "DELETE FROM bands_artists WHERE id = ?";
    var value = bandId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateBandArtist = (bandId, newBand, callback) => {

    var query = "UPDATE bands_artists SET ? WHERE id = ?";;
    var value = [newBand, bandId];
    mysql_db.connection.query(query, value, callback);

}