'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getRole = (roleId, callback) => {

    var query = "SELECT * FROM roles WHERE id= ?";
    var value = roleId;
    mysql_db.connection.query(query, value, callback);

}

exports.listRole = (callback) => {
    var query = "SELECT * FROM roles";
    mysql_db.connection.query(query, callback);
}

exports.createRole = (role, callback) => {

    var query = "INSERT INTO roles SET ?";
    var value = role
    mysql_db.connection.query(query, value, callback);

}

exports.deleteRole = (roleId, callback) => {

    var query = "DELETE FROM roles WHERE id = ?";
    var value = roleId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateRole = (roleId, newTyperole, callback) => {

    var query = "UPDATE roles SET ? WHERE id = ?";;
    var value = [newTyperole, roleId];
    mysql_db.connection.query(query, value, callback);

}