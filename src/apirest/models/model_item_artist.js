'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getItemArtist = (itemId, callback) => {

    var query = "SELECT * FROM items_artists WHERE id= ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.listItemArtist = (callback) => {

    var query = "SELECT * FROM items_artists";
    mysql_db.connection.query(query, callback);
    
}

exports.createItemArtist = (item, callback) => {

    var query = "INSERT INTO items_artists SET ?";
    var value = item
    mysql_db.connection.query(query, value, callback);

}

exports.deleteItemArtist = (itemId, callback) => {

    var query = "DELETE FROM items_artists WHERE id = ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateItemArtist = (itemId, newItem, callback) => {

    var query = "UPDATE items_artists SET ? WHERE id = ?";;
    var value = [newItem, itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemArtistFromItem = (itemId, callback) => {

    var query = "SELECT * FROM items_artists WHERE item_id = ?";
    var value = [itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemArtistFromArtist = (artistId, callback) => {

    var query = "SELECT * FROM items_artists WHERE artist_id = ?";
    var value = [artistId];
    mysql_db.connection.query(query, value, callback);

}