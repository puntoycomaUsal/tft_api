'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getTag = (tagId, callback) => {

    var query = "SELECT * FROM tags WHERE id= ?";
    var value = tagId;
    mysql_db.connection.query(query, value, callback);

}

exports.listTag = (callback) => {
    var query = "SELECT * FROM tags";
    mysql_db.connection.query(query, callback);
}

exports.createTag = (tag, callback) => {

    var query = "INSERT INTO tags SET ?";
    var value = tag
    mysql_db.connection.query(query, value, callback);

}

exports.deleteTag = (tagId, callback) => {

    var query = "DELETE FROM tags WHERE id = ?";
    var value = tagId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateTag = (tagId, newTag, callback) => {

    var query = "UPDATE tags SET ? WHERE id = ?";;
    var value = [newTag, tagId];
    mysql_db.connection.query(query, value, callback);

}

exports.getAlbumsFromTag = (tagId, callback) => {

    var query = "SELECT * FROM albums WHERE id IN (SELECT album_id FROM tags_albums WHERE tag_id = ?)";;
    var value = [tagId];
    mysql_db.connection.query(query, value, callback);

}

exports.getBandsFromTag = (tagId, callback) => {

    var query = "SELECT * FROM bands WHERE id IN (SELECT band_id FROM tags_bands WHERE tag_id = ?)";;
    var value = [tagId];
    mysql_db.connection.query(query, value, callback);

}