'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getBand = (bandId, callback) => {

    var query = "SELECT * FROM bands WHERE id= ?";
    var value = bandId;
    mysql_db.connection.query(query, value, callback);

}

exports.listBand = (callback) => {
    var query = "SELECT * FROM bands";
    mysql_db.connection.query(query, callback);
}

exports.createBand = (band, callback) => {

    var query = "INSERT INTO bands SET ?";
    var value = band
    mysql_db.connection.query(query, value, callback);

}

exports.deleteBand = (bandId, callback) => {

    var query = "DELETE FROM bands WHERE id = ?";
    var value = bandId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateBand = (bandId, newBand, callback) => {

    var query = "UPDATE bands SET ? WHERE id = ?";;
    var value = [newBand, bandId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemsForBand = (bandId, callback) => {

    var query = "SELECT * FROM items WHERE id IN (SELECT item_id FROM items_bands WHERE band_id = ?)";
    var value = [bandId];
    mysql_db.connection.query(query, value, callback);

}

exports.getTagsForBand = (bandId, callback) => {

    var query = "SELECT * FROM tags WHERE id IN (SELECT tag_id FROM tags_bands WHERE band_id = ?)";
    var value = [bandId];
    mysql_db.connection.query(query, value, callback);

}

exports.getArtistsForBand = (bandId, callback) => {

    var query = "SELECT * FROM artists WHERE id IN (SELECT artist_id FROM bands_artists WHERE band_id = ?)";
    var value = [bandId];
    mysql_db.connection.query(query, value, callback);

}