'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getItemAlbum = (itemId, callback) => {

    var query = "SELECT * FROM items_albums WHERE id= ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.listItemAlbum = (callback) => {

    var query = "SELECT * FROM items_albums";
    mysql_db.connection.query(query, callback);
    
}

exports.createItemAlbum = (item, callback) => {

    var query = "INSERT INTO items_albums SET ?";
    var value = item
    mysql_db.connection.query(query, value, callback);

}

exports.deleteItemAlbum = (itemId, callback) => {

    var query = "DELETE FROM items_albums WHERE id = ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateItemAlbum = (itemId, newItem, callback) => {

    var query = "UPDATE items_albums SET ? WHERE id = ?";;
    var value = [newItem, itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemAlbumFromItem = (itemId, callback) => {

    var query = "SELECT * FROM items_albums WHERE item_id = ?";
    var value = [itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemAlbumFromAlbum = (albumId, callback) => {

    var query = "SELECT * FROM items_albums WHERE album_id = ?";
    var value = [albumId];
    mysql_db.connection.query(query, value, callback);

}