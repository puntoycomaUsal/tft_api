const mongoose = require('mongoose');

const newCollectionSchema = mongoose.Schema({
    _id:{
        type: String,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    date:{
        type: Date,
        required: true
    },
    description:{
        type: String,
        required: true
    }
});

const NewCollection = module.exports = mongoose.model("newcollections", newCollectionSchema);

module.exports.listNewCollections = (callback) => {
    NewCollection.find({},callback);
}

module.exports.getNewCollection = (id, callback) => {
    NewCollection.findById(id, callback);
}

module.exports.createNewCollection = (newCollection, callback) => {
    NewCollection.create(newCollection, callback);
}

module.exports.updateNewCollection = (id, newCollection, options, callback) => {
    var query = {_id: id};
    var update = {
        title: newCollection.title,
        description: newCollection.description
    }
    NewCollection.findOneAndUpdate(query, update, options, callback);
}

module.exports.deleteNewCollection = (id, callback) => {
    var query = {_id: id};
    NewCollection.remove(query, callback);
}
