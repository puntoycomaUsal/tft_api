const mongoose = require('mongoose');

const bandCollectionSchema = mongoose.Schema({
    _id:{
        type: String,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    }
});

const BandCollection = module.exports = mongoose.model("bandcollections", bandCollectionSchema);

module.exports.listBandCollections = (callback) => {
    BandCollection.find({},callback);
}

module.exports.getBandCollection = (id, callback) => {
    BandCollection.findById(id, callback);
}

module.exports.createBandCollection = (bandCollection, callback) => {
    BandCollection.create(bandCollection, callback);
}

module.exports.updateBandCollection = (id, bandCollection, options, callback) => {
    var query = {_id: id};
    var update = {
        title: bandCollection.title,
        description: bandCollection.description
    }
    BandCollection.findOneAndUpdate(query, update, options, callback);
}

module.exports.deleteBandCollection = (id, callback) => {
    var query = {_id: id};
    BandCollection.remove(query, callback);
}
