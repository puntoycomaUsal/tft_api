'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getTypeband = (typebandId, callback) => {

    var query = "SELECT * FROM typebands WHERE id= ?";
    var value = typebandId;
    mysql_db.connection.query(query, value, callback);

}

exports.listTypeband = (callback) => {
    var query = "SELECT * FROM typebands";
    mysql_db.connection.query(query, callback);
}

exports.createTypeband = (typeband, callback) => {

    var query = "INSERT INTO typebands SET ?";
    var value = typeband
    mysql_db.connection.query(query, value, callback);

}

exports.deleteTypeband = (typebandId, callback) => {

    var query = "DELETE FROM typebands WHERE id = ?";
    var value = typebandId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateTypeband = (typebandId, newTypetypeband, callback) => {

    var query = "UPDATE typebands SET ? WHERE id = ?";;
    var value = [newTypetypeband, typebandId];
    mysql_db.connection.query(query, value, callback);

}