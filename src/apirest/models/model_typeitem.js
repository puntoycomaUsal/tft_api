'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getTypeitem = (typeitemId, callback) => {

    var query = "SELECT * FROM typeitems WHERE id= ?";
    var value = typeitemId;
    mysql_db.connection.query(query, value, callback);

}

exports.listTypeitem = (callback) => {
    var query = "SELECT * FROM typeitems";
    mysql_db.connection.query(query, callback);
}

exports.createTypeitem = (typeitem, callback) => {

    var query = "INSERT INTO typeitems SET ?";
    var value = typeitem
    mysql_db.connection.query(query, value, callback);

}

exports.deleteTypeitem = (typeitemId, callback) => {

    var query = "DELETE FROM typeitems WHERE id = ?";
    var value = typeitemId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateTypeitem = (typeitemId, newTypetypeitem, callback) => {

    var query = "UPDATE typeitems SET ? WHERE id = ?";;
    var value = [newTypetypeitem, typeitemId];
    mysql_db.connection.query(query, value, callback);

}