'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getArtist = (artistId, callback) => {

    var query = "SELECT * FROM artists WHERE id= ?";
    var value = artistId;
    mysql_db.connection.query(query, value, callback);

}

exports.listArtist = (callback) => {
    var query = "SELECT * FROM artists";
    mysql_db.connection.query(query, callback);
}

exports.createArtist = (artist, callback) => {

    var query = "INSERT INTO artists SET ?";
    var value = artist
    mysql_db.connection.query(query, value, callback);

}

exports.deleteArtist = (artistId, callback) => {

    var query = "DELETE FROM artists WHERE id = ?";
    var value = artistId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateArtist = (artistId, newArtist, callback) => {

    var query = "UPDATE artists SET ? WHERE id = ?";;
    var value = [newArtist, artistId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemsForArtist = (artistId, callback) => {
    var query = "SELECT * FROM items WHERE id IN (SELECT item_id FROM items_artists WHERE artist_id = ?)";
    var value = [artistId];
    mysql_db.connection.query(query, value, callback);

}

exports.getBandForArtist = (artistId, callback) => {
    var query = "SELECT * FROM bands WHERE id IN (SELECT band_id FROM bands_artists WHERE artist_id = ?)";
    var value = [artistId];
    mysql_db.connection.query(query, value, callback);

}