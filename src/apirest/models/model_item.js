'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getItem = (itemId, callback) => {

    var query = "SELECT * FROM items WHERE id= ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.listItem = (callback) => {
    var query = "SELECT * FROM items";
    mysql_db.connection.query(query, callback);
}

exports.createItem = (item, callback) => {

    var query = "INSERT INTO items SET ?";
    var value = item
    mysql_db.connection.query(query, value, callback);

}

exports.deleteItem = (itemId, callback) => {

    var query = "DELETE FROM items WHERE id = ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateItem = (itemId, newItem, callback) => {

    var query = "UPDATE items SET ? WHERE id = ?";;
    var value = [newItem, itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getAlbumForItem = (itemId, callback) => {

    var query = "SELECT * FROM albums WHERE id IN (SELECT album_id FROM items_albums WHERE item_id = ?)";
    var value = [itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getArtistForItem = (itemId, callback) => {

    var query = "SELECT * FROM artists WHERE id IN (SELECT artist_id FROM items_artists WHERE item_id = ?)";
    var value = [itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getBandForItem = (itemId, callback) => {

    var query = "SELECT * FROM bands WHERE id IN (SELECT band_id FROM items_bands WHERE item_id = ?)";
    var value = [itemId];
    mysql_db.connection.query(query, value, callback);

}