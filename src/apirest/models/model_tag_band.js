'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getTagBand = (tagId, callback) => {

    var query = "SELECT * FROM tags_bands WHERE id= ?";
    var value = tagId;
    mysql_db.connection.query(query, value, callback);

}

exports.listTagBand = (callback) => {
    var query = "SELECT * FROM tags_bands";
    mysql_db.connection.query(query, callback);
}

exports.createTagBand = (tag, callback) => {

    var query = "INSERT INTO tags_bands SET ?";
    var value = tag
    mysql_db.connection.query(query, value, callback);

}

exports.deleteTagBand = (tagId, callback) => {

    var query = "DELETE FROM tags_bands WHERE id = ?";
    var value = tagId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateTagBand = (tagId, newTag, callback) => {

    var query = "UPDATE tags_bands SET ? WHERE id = ?";;
    var value = [newTag, tagId];
    mysql_db.connection.query(query, value, callback);

}