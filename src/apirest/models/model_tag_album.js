'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getTagAlbum = (tagId, callback) => {

    var query = "SELECT * FROM tags_albums WHERE id= ?";
    var value = tagId;
    mysql_db.connection.query(query, value, callback);

}

exports.listTagAlbum = (callback) => {
    var query = "SELECT * FROM tags_albums";
    mysql_db.connection.query(query, callback);
}

exports.createTagAlbum = (tag, callback) => {

    var query = "INSERT INTO tags_albums SET ?";
    var value = tag
    mysql_db.connection.query(query, value, callback);

}

exports.deleteTagAlbum = (tagId, callback) => {

    var query = "DELETE FROM tags_albums WHERE id = ?";
    var value = tagId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateTagAlbum = (tagId, newTag, callback) => {

    var query = "UPDATE tags_albums SET ? WHERE id = ?";;
    var value = [newTag, tagId];
    mysql_db.connection.query(query, value, callback);

}