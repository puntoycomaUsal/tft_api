'use strict';
const mysql     = require('mysql');
const mysql_db  = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mysql.js`);

exports.getItemBand = (itemId, callback) => {

    var query = "SELECT * FROM items_bands WHERE id= ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.listItemBand = (callback) => {

    var query = "SELECT * FROM items_bands";
    mysql_db.connection.query(query, callback);
    
}

exports.createItemBand = (item, callback) => {

    var query = "INSERT INTO items_bands SET ?";
    var value = item
    mysql_db.connection.query(query, value, callback);

}

exports.deleteItemBand = (itemId, callback) => {

    var query = "DELETE FROM items_bands WHERE id = ?";
    var value = itemId;
    mysql_db.connection.query(query, value, callback);

}

exports.updateItemBand = (itemId, newItem, callback) => {

    var query = "UPDATE items_bands SET ? WHERE id = ?";;
    var value = [newItem, itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemBandFromItem = (itemId, callback) => {

    var query = "SELECT * FROM items_bands WHERE item_id = ?";
    var value = [itemId];
    mysql_db.connection.query(query, value, callback);

}

exports.getItemBandFromBand = (bandId, callback) => {

    var query = "SELECT * FROM items_bands WHERE band_id = ?";
    var value = [bandId];
    mysql_db.connection.query(query, value, callback);

}