const mongoose = require('mongoose');

const artistCollectionSchema = mongoose.Schema({
    _id:{
        type: String,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    }
});

const ArtistCollection = module.exports = mongoose.model("artistcollections", artistCollectionSchema);

module.exports.listArtistCollections = (callback) => {
    ArtistCollection.find({},callback);
}

module.exports.getArtistCollection = (id, callback) => {
    ArtistCollection.findById(id, callback);
}

module.exports.createArtistCollection = (artistCollection, callback) => {
    ArtistCollection.create(artistCollection, callback);
}

module.exports.updateArtistCollection = (id, artistCollection, options, callback) => {
    var query = {_id: id};
    var update = {
        title: artistCollection.title,
        description: artistCollection.description
    }
    ArtistCollection.findOneAndUpdate(query, update, options, callback);
}

module.exports.deleteArtistCollection = (id, callback) => {
    var query = {_id: id};
    ArtistCollection.remove(query, callback);
}
