const mongoose = require('mongoose');

const albumCollectionSchema = mongoose.Schema({
    _id:{
        type: String,
        required: true
    },
    title:{
        type: String,
        required: true
    },
    description:{
        type: String,
        required: true
    }
});

const AlbumCollection = module.exports = mongoose.model("albumcollections", albumCollectionSchema);

module.exports.listAlbumCollections = (callback) => {
    AlbumCollection.find({},callback);
}

module.exports.getAlbumCollection = (id, callback) => {
    AlbumCollection.findById(id, callback);
}

module.exports.createAlbumCollection = (albumCollection, callback) => {
    AlbumCollection.create(albumCollection, callback);
}

module.exports.updateAlbumCollection = (id, albumCollection, options, callback) => {
    var query = {_id: id};
    var update = {
        title: albumCollection.title,
        description: albumCollection.description
    }
    AlbumCollection.findOneAndUpdate(query, update, options, callback);
}

module.exports.deleteAlbumCollection = (id, callback) => {
    var query = {_id: id};
    AlbumCollection.remove(query, callback);
}
