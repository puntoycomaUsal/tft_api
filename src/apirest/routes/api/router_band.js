'use strict';
const express     = require('express');
const router      = express.Router();
const bandModel = require(`${process.env.PATH_BASE}/models/model_band`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef band
 * @property {number} typeband_id - Type al que pertenece la banda
 * @property {string} name - Nombre de la banda
 * @property {string} foundation_date - Fecha de creación de la banda
 * @property {string} web - Web de la banda
 */


/**
 * Listar todas las bandas
 * @route GET /band
 * @group Etiqueta Band - End-point para la entidad Band
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/band', function(req, res, next) {
    
    reqinfo.maininfo(req);
    bandModel.listBand((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una banda
 * @route GET /band/{id}
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id del banda que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/band/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let bandId = req.params.id;
    bandModel.getBand(bandId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consulta todos los items de una banda
 * @route GET /band/{id}/items
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id de la banda cuyos items se quieren conocer
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/band/:id/items', function(req, res, next) {

    reqinfo.maininfo(req);
    let bandId = req.params.id;
    bandModel.getItemsForBand(bandId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(200).send({type: 1, message: "OK", data: result});
        else console.log(result);
    })

});

/**
 * Consulta todos los tags de una banda
 * @route GET /band/{id}/tags
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id de la banda cuyos tags se quieren conocer
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/band/:id/tags', function(req, res, next) {

    reqinfo.maininfo(req);
    let bandId = req.params.id;
    bandModel.getTagsForBand(bandId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(200).send({type: 1, message: "OK", data: result});
        else console.log(result);
    })

});

/**
 * Consulta todos los artistas de una banda
 * @route GET /band/{id}/artist
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id de la banda cuyos artistas se quieren conocer
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/band/:id/artist', function(req, res, next) {

    reqinfo.maininfo(req);
    let bandId = req.params.id;
    bandModel.getArtistsForBand(bandId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(200).send({type: 1, message: "OK", data: result});
        else console.log(result);
    })

});

/**
 * Crear una banda
 * @route POST /band
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {band.model} band.body.required - Banda que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/band', function(req, res, next) {

    reqinfo.maininfo(req);
    bandModel.createBand(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una banda
 * @route DELETE /band/{id}
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id de la banda que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/band/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let bandId = req.params.id;
        bandModel.deleteBand(bandId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una banda
 * @route PUT /band/{id}
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id del banda que se desea actualizar
 * @param {band.model} band.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/band/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let bandId = req.params.id;
        bandModel.updateBand(bandId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
