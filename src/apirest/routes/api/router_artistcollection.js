'use strict';
const express              = require('express');
const router               = express.Router();
const artistcollectionModel = require(`${process.env.PATH_BASE}/models/model_artistcollection`);
const utils                = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo              = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef ArtistCollection
 * @property {string} _id - Id que identifica el documento en la base de datos mongoDB
 * @property {string} title - Titulo del artist
 * @property {string} description - Descripción del artist
 */

/**
 * Listar todos los artistcollection
 * @route GET /artistcollection
 * @group Etiqueta ArtistCollection - End-point para la entidad artistcollection
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/artistcollection', function(req, res, next) {
    
    reqinfo.maininfo(req);
    artistcollectionModel.listArtistCollections((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar un artistcollection
 * @route GET /artistcollection/{id}
 * @group Etiqueta ArtistCollection - End-point para la entidad artistcollection
 * @param {string} id.path.required - Id del ArtistCollection que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - No se ha encontrado ningun objeto con ese identificador
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/artistcollection/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let artistcollectionId = req.params.id;
    artistcollectionModel.getArtistCollection(artistcollectionId, (err, result) => {

        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(200).send({type: 1, message: "OK", data: result});
        else res.status(404).send({type: 1, message: "Not found", data: []}); // No existe ese objeto
    })

});

/**
 * Crear un nuevo artist
 * @route POST /artistcollection
 * @group Etiqueta ArtistCollection - End-point para la entidad Artist
 * @param {ArtistCollection.model} artistcollection.body.required - Artist que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/artistcollection', function(req, res, next) {

    reqinfo.maininfo(req);
    artistcollectionModel.createArtistCollection(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar un ArtistCollection
 * @route DELETE /artistcollection/{id}
 * @group Etiqueta ArtistCollection - End-point para la entidad artistcollection
 * @param {string} id.path.required - Id del artistcollection que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/artistcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let artistcollectionId = req.params.id;
        artistcollectionModel.deleteArtistCollection(artistcollectionId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

/**
 * Consultar un ArtistCollection
 * @route PUT /artistcollection/{id}
 * @group Etiqueta ArtistCollection - End-point para la entidad ArtistCollection
 * @param {string} id.path.required - Id del artist que se desea actualizar
 * @param {ArtistCollection.model} ArtistCollection.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/artistcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let artistId = req.params.id;
        artistcollectionModel.updateArtistCollection(artistId, req.body, null, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

module.exports = router;
