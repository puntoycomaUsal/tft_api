'use strict';
const express     = require('express');
const router      = express.Router();
const typeitemModel = require(`${process.env.PATH_BASE}/models/model_typeitem`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef typeitem
 * @property {string} name - Nombre
 */


/**
 * Listar todas las typeitems
 * @route GET /typeitem
 * @group Etiqueta Typeitem - End-point para la entidad typeitem
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/typeitem', function(req, res, next) {
    
    reqinfo.maininfo(req);
    typeitemModel.listTypeitem((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una typeitem
 * @route GET /typeitem/{id}
 * @group Etiqueta Typeitem - End-point para la entidad typeitem
 * @param {string} id.path.required - Id del typeitem que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/typeitem/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let typeitemId = req.params.id;
    typeitemModel.getTypeitem(typeitemId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear una typeitem
 * @route POST /typeitem
 * @group Etiqueta Typeitem - End-point para la entidad typeitem
 * @param {typeitem.model} typeitem.body.required - Banda que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/typeitem', function(req, res, next) {

    reqinfo.maininfo(req);
    typeitemModel.createTypeitem(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una typeitem
 * @route DELETE /typeitem/{id}
 * @group Etiqueta Typeitem - End-point para la entidad typeitem
 * @param {string} id.path.required - Id de la typeitem que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/typeitem/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let typeitemId = req.params.id;
        typeitemModel.deleteTypeitem(typeitemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una typeitem
 * @route PUT /typeitem/{id}
 * @group Etiqueta Typeitem - End-point para la entidad typeitem
 * @param {string} id.path.required - Id del typeitem que se desea actualizar
 * @param {typeitem.model} typeitem.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/typeitem/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let typeitemId = req.params.id;
        typeitemModel.updateTypeitem(typeitemId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
