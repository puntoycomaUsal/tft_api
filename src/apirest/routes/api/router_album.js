'use strict';
const express     = require('express');
const router      = express.Router();
const albumModel = require(`${process.env.PATH_BASE}/models/model_album`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef album
 * @property {string} name - Nombre del album
 * @property {string} publication_date - Fecha de publicación
 * @property {string} web - Url del album
 */


/**
 * Listar todos los albums
 * @route GET /album
 * @group Etiqueta Album - End-point para la entidad Album
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/album', function(req, res, next) {
    
    reqinfo.maininfo(req);
    albumModel.listAlbum((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar un album
 * @route GET /album/{id}
 * @group Etiqueta Album - End-point para la entidad Album
 * @param {string} id.path.required - Id del album que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/album/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let albumId = req.params.id;
    albumModel.getAlbum(albumId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar los items de un album
 * @route GET /album/{id}/items
 * @group Etiqueta Album - End-point para la entidad Album
 * @param {string} id.path.required - Id del album que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/album/:id/items', function(req, res, next) {

    reqinfo.maininfo(req);
    let albumId = req.params.id;
    albumModel.getItemsForAlbum(albumId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar los tags de un album
 * @route GET /album/{id}/tags
 * @group Etiqueta Album - End-point para la entidad Album
 * @param {string} id.path.required - Id del album cuyos tags se quieren consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/album/:id/tags', function(req, res, next) {

    reqinfo.maininfo(req);
    let albumId = req.params.id;
    albumModel.getTagsForAlbum(albumId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear un nuevo album
 * @route POST /album
 * @group Etiqueta Album - End-point para la entidad Album
 * @param {album.model} album.body.required - Album que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/album', function(req, res, next) {

    reqinfo.maininfo(req);
    albumModel.createAlbum(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar un album
 * @route DELETE /album/{id}
 * @group Etiqueta Album - End-point para la entidad Album
 * @param {string} id.path.required - Id del albuma que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Album no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/album/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let albumId = req.params.id;
        albumModel.deleteAlbum(albumId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Consultar un album
 * @route PUT /album/{id}
 * @group Etiqueta Album - End-point para la entidad Album
 * @param {string} id.path.required - Id del album que se desea actualizar
 * @param {album.model} album.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Albuma no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/album/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let albumId = req.params.id;
        albumModel.updateAlbum(albumId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
