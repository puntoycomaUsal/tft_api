'use strict';
const express         = require('express');
const router          = express.Router();
const tagBandModel   = require(`${process.env.PATH_BASE}/models/model_tag_band`);
const utils           = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo         = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef tag_band
 * @property {number} tag_id - Identificador del tipo de tag
 * @property {number} band_id - Banda que se identifica con dicho tag
 */


/**
 * Listar todas las relaciones entre band y tag
 * @route GET /tag/band
 * @group Etiqueta Tag - End-point para la relación band-tag
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag/band', function(req, res, next) {
    
    reqinfo.maininfo(req);
    tagBandModel.listTagBand((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una relación band-tag
 * @route GET /tag/band/{id}
 * @group Etiqueta Tag - End-point para la entiendad Tag
 * @param {string} id.path.required - Id de la relación
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag/band/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let tagId = req.params.id;
    tagBandModel.getTagBand(tagId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear una nueva relación band-tag
 * @route POST /tag/band
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {tag_band.model} tag.body.required - Nueva relación que se desea crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/tag/band', function(req, res, next) {

    reqinfo.maininfo(req);
    tagBandModel.createTagBand(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una tag
 * @route DELETE /tag/band/{id}
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Tag no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/tag/band/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let tagId = req.params.id;
        tagBandModel.deleteTagBand(tagId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una relación band-tag
 * @route PUT /tag/band/{id}
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea actualizar
 * @param {tag_band.model} tag.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Tag no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/tag/band/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let tagId = req.params.id;
        tagBandModel.updateTagBand(tagId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
