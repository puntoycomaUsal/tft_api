'use strict';
const express         = require('express');
const router          = express.Router();
const itemBandModel   = require(`${process.env.PATH_BASE}/models/model_item_band`);
const utils           = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo         = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef Item_band
 * @property {number} band_id - Type al que pertenece la itema
 * @property {number} item_id - Nombre del item
 * @property {number} typeitem_id - Tipo del item
 * @property {number} position - Fecha de creación de la itema
 */

/**
 * Listar todas las relaciones entre band e item
 * @route GET /item/band
 * @group Etiqueta Item_band - End-point para la relación band-item
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/band', function(req, res, next) {

    reqinfo.maininfo(req);
    itemBandModel.listItemBand((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una relación band-item
 * @route GET /item/band/{id}
 * @group Etiqueta Item_band - End-point para la entiendad Item
 * @param {string} id.path.required - Id de la relación
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/band/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    let itemId = req.params.id;
    itemBandModel.getItemBand(itemId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Permite consultar la relación entre item y band filtrando por uno de los dos campos
 * para poder filtrar por el campo band el de item debe ser -1.
 * @route GET /item/band/{idItem}/{idBand}
 * @group Etiqueta Item_band - End-point para la entidad Item
 * @param {string} idItem.path.required - Id del item que se esta consultando
 * @param {string} idBand.path.required - Id de la banda que se esta consultando
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/band/:idItem/:idBand', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let itemId = req.params['idItem'];
    let bandId = req.params['idBand'];
    
    if (itemId !== '-1') {
        itemBandModel.getItemBandFromItem(itemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) res.status(200).send({type: 1, message: "OK", data: result});
        })
    }else {
        itemBandModel.getItemBandFromBand(bandId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) res.status(200).send({type: 1, message: "OK", data: result});
        })
    }

});

/**
 * Crear una nueva relación band-item
 * @route POST /item/band
 * @group Etiqueta Item_band - End-point para la entidad Item
 * @param {Item_band.model} item.body.required - Item que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/item/band', function(req, res, next) {

    reqinfo.maininfo(req);
    itemBandModel.createItemBand(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una itema
 * @route DELETE /item/band/{id}
 * @group Etiqueta Item_band - End-point para la entidad Item
 * @param {string} id.path.required - Id de la relación que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Relación no encontrada
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/item/band/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let itemId = req.params.id;
        itemBandModel.deleteItemBand(itemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una relación band-item
 * @route PUT /item/band/{id}
 * @group Etiqueta Item_band - End-point para la entidad Item
 * @param {string} id.path.required - Id de la relazión que se va actualizar
 * @param {Item_band.model} item.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Relación no encontrada
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/item/band/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let itemId = req.params.id;
        itemBandModel.updateItemBand(itemId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
