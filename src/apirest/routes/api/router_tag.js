'use strict';
const express     = require('express');
const router      = express.Router();
const tagModel    = require(`${process.env.PATH_BASE}/models/model_tag`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef tag
 * @property {string} name - Nombre del tag 
 */


/**
 * Listar todos los tags
 * @route GET /tag
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag', function(req, res, next) {
    
    reqinfo.maininfo(req);
    tagModel.listTag((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar un tag
 * @route GET /tag/{id}
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let tagId = req.params.id;
    tagModel.getTag(tagId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar todos los albums que tienen el tag indicado
 * @route GET /tag/{id}/album
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag/:id/album', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let tagId = req.params.id;
    tagModel.getAlbumsFromTag(tagId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar todas las bandas que tienen el tag indicado
 * @route GET /tag/{id}/band
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag/:id/band', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let tagId = req.params.id;
    tagModel.getBandsFromTag(tagId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});


/**
 * Crear un nuevo tag
 * @route POST /tag
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {tag.model} tag.body.required - Taga que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/tag', function(req, res, next) {

    reqinfo.maininfo(req);
    tagModel.createTag(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar un tag
 * @route DELETE /tag/{id}
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Taga no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/tag/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let tagId = req.params.id;
        tagModel.deleteTag(tagId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Consultar un tag
 * @route PUT /tag/{id}
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea actualizar
 * @param {tag.model} tag.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Taga no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/tag/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let tagId = req.params.id;
        tagModel.updateTag(tagId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
