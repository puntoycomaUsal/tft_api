'use strict';
const express         = require('express');
const router          = express.Router();
const itemAlbumModel = require(`${process.env.PATH_BASE}/models/model_item_album`);
const utils           = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo         = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef Item_album
 * @property {number} album_id - Type al que pertenece la itema
 * @property {number} item_id - Nombre de la itema
 * @property {number} typeitem_id - Tipo del item
 * @property {number} position - Fecha de creación de la itema
 */


/**
 * Listar todas las relaciones entre album e item
 * @route GET /item/album
 * @group Etiqueta Item - End-point para la relación album-item
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/album', function(req, res, next) {
    
    reqinfo.maininfo(req);
    itemAlbumModel.listItemAlbum((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una relación album-item
 * @route GET /item/album/{id}
 * @group Etiqueta Item_album - End-point para la entiendad Item
 * @param {string} id.path.required - Id de la relación
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/album/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let itemId = req.params.id;
    itemAlbumModel.getItemAlbum(itemId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Permite consultar la relación entre item y album filtrando por uno de los dos campos
 * para poder filtrar por el campo album el de item debe ser -1.
 * @route GET /item/album/{idItem}/{idAlbum}
 * @group Etiqueta Item_album - End-point para la entidad Item
 * @param {string} idItem.path.required - Id del item que se esta consultando
 * @param {string} idAlbum.path.required - Id del album que se esta consultando
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/album/:idItem/:idAlbum', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let itemId = req.params['idItem'];
    let albumdId = req.params['idAlbum'];
    
    if (itemId !== '-1') {
        itemAlbumModel.getItemAlbumFromItem(itemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) res.status(200).send({type: 1, message: "OK", data: result});
        })
    }else {
        itemAlbumModel.getItemAlbumFromAlbum(albumdId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) res.status(200).send({type: 1, message: "OK", data: result});
        })
    }

});

/**
 * Crear una nueva relación album-item
 * @route POST /item/album
 * @group Etiqueta Item_album - End-point para la entidad Item
 * @param {Item_album.model} item.body.required - Item que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/item/album', function(req, res, next) {

    reqinfo.maininfo(req);
    itemAlbumModel.createItemAlbum(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una itema
 * @route DELETE /item/album/{id}
 * @group Etiqueta Item_album - End-point para la entidad Item
 * @param {string} id.path.required - Id de la relación que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Relación no encontrada
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/item/album/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let itemId = req.params.id;
        itemAlbumModel.deleteItemAlbum(itemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una relación album-item
 * @route PUT /item/album/{id}
 * @group Etiqueta Item_album - End-point para la entidad Item
 * @param {string} id.path.required - Id de la relazión que se va actualizar
 * @param {Item_album.model} item.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Relación no encontrada
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/item/album/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let itemId = req.params.id;
        itemAlbumModel.updateItemAlbum(itemId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
