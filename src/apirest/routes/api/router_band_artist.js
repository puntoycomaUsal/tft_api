'use strict';
const express         = require('express');
const router          = express.Router();
const bandArtistModel = require(`${process.env.PATH_BASE}/models/model_band_artist`);
const utils           = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo         = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef band_artist
 * @property {number} artist_id - Type al que pertenece la banda
 * @property {number} band_id - Nombre de la banda
 * @property {number} role_id - Fecha de creación de la banda
 * @property {string} start_date - Web de la banda
 * @property {string} end_date - Web de la banda
 */


/**
 * Listar todas las relaciones entre artist y band
 * @route GET /band/artist
 * @group Etiqueta Band - End-point para la relación artist-band
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/band/artist', function(req, res, next) {
    
    reqinfo.maininfo(req);
    bandArtistModel.listBandArtist((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una relación artista-banda
 * @route GET /band/artist/{id}
 * @group Etiqueta Band - End-point para la entiendad Band
 * @param {string} id.path.required - Id de la relación
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/band/artist/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let bandId = req.params.id;
    bandArtistModel.getBandArtist(bandId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear una nueva relación artist-band
 * @route POST /band/artist
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {band_artist.model} band.body.required - Banda que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/band/artist', function(req, res, next) {

    reqinfo.maininfo(req);
    bandArtistModel.createBandArtist(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una banda
 * @route DELETE /band/artist/{id}
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id de la banda que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/band/artist/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let bandId = req.params.id;
        bandArtistModel.deleteBandArtist(bandId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una relación artist-band
 * @route PUT /band/artist/{id}
 * @group Etiqueta Band - End-point para la entidad Band
 * @param {string} id.path.required - Id del banda que se desea actualizar
 * @param {band_artist.model} band.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/band/artist/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let bandId = req.params.id;
        bandArtistModel.updateBandArtist(bandId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
