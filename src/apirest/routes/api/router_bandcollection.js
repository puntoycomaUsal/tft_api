'use strict';
const express              = require('express');
const router               = express.Router();
const bandcollectionModel = require(`${process.env.PATH_BASE}/models/model_bandcollection`);
const utils                = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo              = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef BandCollection
 * @property {string} _id - Id que identifica el documento en la base de datos mongoDB
 * @property {string} title - Titulo del band
 * @property {string} description - Descripción del band
 */

/**
 * Listar todos los bandcollection
 * @route GET /bandcollection
 * @group Etiqueta BandCollection - End-point para la entidad bandcollection
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/bandcollection', function(req, res, next) {
    
    reqinfo.maininfo(req);
    bandcollectionModel.listBandCollections((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar un bandcollection
 * @route GET /bandcollection/{id}
 * @group Etiqueta BandCollection - End-point para la entidad bandcollection
 * @param {string} id.path.required - Id del BandCollection que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - No se ha encontrado ningun objeto con ese identificador
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/bandcollection/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let bandcollectionId = req.params.id;
    bandcollectionModel.getBandCollection(bandcollectionId, (err, result) => {

        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(200).send({type: 1, message: "OK", data: result});
        else res.status(404).send({type: 1, message: "Not found", data: []}); // No existe ese objeto
    })

});

/**
 * Crear un nuevo band
 * @route POST /bandcollection
 * @group Etiqueta BandCollection - End-point para la entidad Band
 * @param {BandCollection.model} bandcollection.body.required - Band que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/bandcollection', function(req, res, next) {

    reqinfo.maininfo(req);
    bandcollectionModel.createBandCollection(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar un BandCollection
 * @route DELETE /bandcollection/{id}
 * @group Etiqueta BandCollection - End-point para la entidad bandcollection
 * @param {string} id.path.required - Id del bandcollection que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/bandcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let bandcollectionId = req.params.id;
        bandcollectionModel.deleteBandCollection(bandcollectionId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

/**
 * Consultar un BandCollection
 * @route PUT /bandcollection/{id}
 * @group Etiqueta BandCollection - End-point para la entidad BandCollection
 * @param {string} id.path.required - Id del band que se desea actualizar
 * @param {BandCollection.model} BandCollection.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/bandcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let bandId = req.params.id;
        bandcollectionModel.updateBandCollection(bandId, req.body, null, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

module.exports = router;
