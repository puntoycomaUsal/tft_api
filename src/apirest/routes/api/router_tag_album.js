'use strict';
const express         = require('express');
const router          = express.Router();
const tagAlbumModel   = require(`${process.env.PATH_BASE}/models/model_tag_album`);
const utils           = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo         = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef tag_album
 * @property {number} tag_id - Type al que pertenece la taga
 * @property {number} album_id - Nombre de la tagaa
 */


/**
 * Listar todas las relaciones entre album y tag
 * @route GET /tag/album
 * @group Etiqueta Tag - End-point para la relación album-tag
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag/album', function(req, res, next) {
    
    reqinfo.maininfo(req);
    tagAlbumModel.listTagAlbum((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una relación album-tag
 * @route GET /tag/album/{id}
 * @group Etiqueta Tag - End-point para la entiendad Tag
 * @param {string} id.path.required - Id de la relación
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/tag/album/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let tagId = req.params.id;
    tagAlbumModel.getTagAlbum(tagId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear una nueva relación album-tag
 * @route POST /tag/album
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {tag_album.model} tag.body.required - Nueva relación que se desea crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/tag/album', function(req, res, next) {

    reqinfo.maininfo(req);
    tagAlbumModel.createTagAlbum(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una tag
 * @route DELETE /tag/album/{id}
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id de la tag que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Tag no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/tag/album/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let tagId = req.params.id;
        tagAlbumModel.deleteTagAlbum(tagId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una relación album-tag
 * @route PUT /tag/album/{id}
 * @group Etiqueta Tag - End-point para la entidad Tag
 * @param {string} id.path.required - Id del tag que se desea actualizar
 * @param {tag_album.model} tag.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Taga no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/tag/album/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let tagId = req.params.id;
        tagAlbumModel.updateTagAlbum(tagId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
