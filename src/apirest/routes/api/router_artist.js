'use strict';
const express     = require('express');
const router      = express.Router();
const artistModel = require(`${process.env.PATH_BASE}/models/model_artist`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef artist
 * @property {string} first_name - first_name del artista que se desea crear
 * @property {string} last_name - second_name del artista que se desea crear
 * @property {string} birth_date - birth_date del artista que se desea crear
 * @property {string} birth_place - birth_place del artista que se desea crear
 * @property {string} biography - biography del artista que se desea crear
 * @property {string} web - web del artista que se desea crear
 */


/**
 * Listar todos los usuarios
 * @route GET /artist
 * @group Etiqueta Artist - End-point para la entidad Artist
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/artist', function(req, res, next) {
    
    reqinfo.maininfo(req);
    artistModel.listArtist((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar un usuario
 * @route GET /artist/{id}
 * @group Etiqueta Artist - End-point para la entidad Artist
 * @param {string} id.path.required - Id del artista que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/artist/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let artistId = req.params.id;
    artistModel.getArtist(artistId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consulta todos los items de un artista
 * @route GET /artist/{id}/items
 * @group Etiqueta Artist - End-point para la entidad Artist
 * @param {string} id.path.required - Id del artist cuyos items se quieren conocer
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/artist/:id/items', function(req, res, next) {

    reqinfo.maininfo(req);
    let artistId = req.params.id;
    artistModel.getItemsForArtist(artistId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consulta todos la banda de un artista
 * @route GET /artist/{id}/bands
 * @group Etiqueta Artist - End-point para la entidad Artist
 * @param {string} id.path.required - Id del artist cuya banda se quiere consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/artist/:id/bands', function(req, res, next) {

    reqinfo.maininfo(req);
    let artistId = req.params.id;
    artistModel.getBandForArtist(artistId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear un nuevo usuario
 * @route POST /artist
 * @group Etiqueta Artist - End-point para la entidad Artist
 * @param {artist.model} artist.body.required - Artista que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/artist', function(req, res, next) {

    reqinfo.maininfo(req);
    artistModel.createArtist(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar un usuario
 * @route DELETE /artist/{id}
 * @group Etiqueta Artist - End-point para la entidad Artist
 * @param {string} id.path.required - Id del artista que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Artista no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/artist/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let artistId = req.params.id;
        artistModel.deleteArtist(artistId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Consultar un usuario
 * @route PUT /artist/{id}
 * @group Etiqueta Artist - End-point para la entidad Artist
 * @param {string} id.path.required - Id del artista que se desea actualizar
 * @param {artist.model} artist.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Artista no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/artist/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let artistId = req.params.id;
        artistModel.updateArtist(artistId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
