'use strict';
const express     = require('express');
const router      = express.Router();
const roleModel = require(`${process.env.PATH_BASE}/models/model_role`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef role
 * @property {string} name - Nombre
 */


/**
 * Listar todas los roles
 * @route GET /role
 * @group Etiqueta Role - End-point para la entidad role
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/role', function(req, res, next) {
    
    reqinfo.maininfo(req);
    roleModel.listRole((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar un role
 * @route GET /role/{id}
 * @group Etiqueta Role - End-point para la entidad role
 * @param {string} id.path.required - Id del role que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/role/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let roleId = req.params.id;
    roleModel.getRole(roleId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear un nuevo role
 * @route POST /role
 * @group Etiqueta Role - End-point para la entidad role
 * @param {role.model} role.body.required - Role que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/role', function(req, res, next) {

    reqinfo.maininfo(req);
    roleModel.createRole(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar un role
 * @route DELETE /role/{id}
 * @group Etiqueta Role - End-point para la entidad role
 * @param {string} id.path.required - Id del role que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/role/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let roleId = req.params.id;
        roleModel.deleteRole(roleId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una role
 * @route PUT /role/{id}
 * @group Etiqueta Role - End-point para la entidad role
 * @param {string} id.path.required - Id del rol que se desea actualizar
 * @param {role.model} role.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/role/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let roleId = req.params.id;
        roleModel.updateRole(roleId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
