'use strict';
const express              = require('express');
const router               = express.Router();
const newcollectionModel = require(`${process.env.PATH_BASE}/models/model_newcollection`);
const utils                = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo              = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef NewCollection
 * @property {string} _id - Id que identifica el documento en la base de datos mongoDB
 * @property {string} title - Titulo de la noticia
 * @property {string} date - Descripción de la noticia
 * @property {string} description - Descripción de la noticia
 */

/**
 * Listar todas las noticias
 * @route GET /newcollection
 * @group Etiqueta NewCollection - End-point para la entidad newcollection
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/newcollection', function(req, res, next) {
    
    reqinfo.maininfo(req);
    newcollectionModel.listNewCollections((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una notificia
 * @route GET /newcollection/{id}
 * @group Etiqueta NewCollection - End-point para la entidad newcollection
 * @param {string} id.path.required - Id del NewCollection que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - No se ha encontrado ningun objeto con ese identificador
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/newcollection/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let newcollectionId = req.params.id;
    newcollectionModel.getNewCollection(newcollectionId, (err, result) => {

        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(200).send({type: 1, message: "OK", data: result});
        else res.status(404).send({type: 1, message: "Not found", data: []}); // No existe ese objeto
    })

});

/**
 * Crear una nueva noticia
 * @route POST /newcollection
 * @group Etiqueta NewCollection - End-point para la entidad New
 * @param {NewCollection.model} newcollection.body.required - New que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/newcollection', function(req, res, next) {

    reqinfo.maininfo(req);
    newcollectionModel.createNewCollection(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una notificia
 * @route DELETE /newcollection/{id}
 * @group Etiqueta NewCollection - End-point para la entidad newcollection
 * @param {string} id.path.required - Id del newcollection que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/newcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let newcollectionId = req.params.id;
        newcollectionModel.deleteNewCollection(newcollectionId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

/**
 * Consultar una noticia
 * @route PUT /newcollection/{id}
 * @group Etiqueta NewCollection - End-point para la entidad NewCollection
 * @param {string} id.path.required - Id del new que se desea actualizar
 * @param {NewCollection.model} NewCollection.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/newcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let newId = req.params.id;
        newcollectionModel.updateNewCollection(newId, req.body, null, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

module.exports = router;
