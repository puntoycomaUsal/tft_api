'use strict';
const express              = require('express');
const router               = express.Router();
const albumcollectionModel = require(`${process.env.PATH_BASE}/models/model_albumcollection`);
const utils                = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo              = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef AlbumCollection
 * @property {string} _id - Id que identifica el documento en la base de datos mongoDB
 * @property {string} title - Titulo del album
 * @property {string} description - Descripción del album
 */

/**
 * Listar todos los albumcollection
 * @route GET /albumcollection
 * @group Etiqueta AlbumCollection - End-point para la entidad albumcollection
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/albumcollection', function(req, res, next) {
    
    reqinfo.maininfo(req);
    albumcollectionModel.listAlbumCollections((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar un albumcollection
 * @route GET /albumcollection/{id}
 * @group Etiqueta AlbumCollection - End-point para la entidad albumcollection
 * @param {string} id.path.required - Id del AlbumCollection que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - No se ha encontrado ningun objeto con ese identificador
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/albumcollection/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let albumcollectionId = req.params.id;
    albumcollectionModel.getAlbumCollection(albumcollectionId, (err, result) => {

        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(200).send({type: 1, message: "OK", data: result});
        else res.status(404).send({type: 1, message: "Not found", data: []}); // No existe ese objeto
    })

});

/**
 * Crear un nuevo album
 * @route POST /albumcollection
 * @group Etiqueta AlbumCollection - End-point para la entidad Album
 * @param {AlbumCollection.model} albumcollection.body.required - Album que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/albumcollection', function(req, res, next) {

    reqinfo.maininfo(req);
    albumcollectionModel.createAlbumCollection(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        else if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar un AlbumCollection
 * @route DELETE /albumcollection/{id}
 * @group Etiqueta AlbumCollection - End-point para la entidad albumcollection
 * @param {string} id.path.required - Id del albumcollection que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/albumcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let albumcollectionId = req.params.id;
        albumcollectionModel.deleteAlbumCollection(albumcollectionId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

/**
 * Consultar un AlbumCollection
 * @route PUT /albumcollection/{id}
 * @group Etiqueta AlbumCollection - End-point para la entidad AlbumCollection
 * @param {string} id.path.required - Id del album que se desea actualizar
 * @param {AlbumCollection.model} AlbumCollection.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/albumcollection/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let albumId = req.params.id;
        albumcollectionModel.updateAlbumCollection(albumId, req.body, null, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                res.status(200).send({type: 1, message: 'OK'});
            }
        })
    }

});

module.exports = router;
