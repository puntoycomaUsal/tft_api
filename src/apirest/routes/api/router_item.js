'use strict';
const express     = require('express');
const router      = express.Router();
const itemModel = require(`${process.env.PATH_BASE}/models/model_item`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef item
 * @property {number} typeitem_id - Type al que pertenece el item
 * @property {string} name - Nombre del item
 * @property {number} band_id - Fecha de creación del item
 * @property {string} url - Url del item
 */


/**
 * Listar todas las item
 * @route GET /item
 * @group Etiqueta Item - End-point para la entidad Item
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item', function(req, res, next) {
    
    reqinfo.maininfo(req);
    itemModel.listItem((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una item
 * @route GET /item/{id}
 * @group Etiqueta Item - End-point para la entidad Item
 * @param {string} id.path.required - Id del item que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let itemId = req.params.id;
    itemModel.getItem(itemId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar los albums de un item
 * @route GET /item/{id}/album
 * @group Etiqueta Item - End-point para la entidad Item
 * @param {string} id.path.required - Id del item que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/:id/album', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let albumId = req.params.id;
    itemModel.getAlbumForItem(albumId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar los artistas de un item
 * @route GET /item/{id}/artist
 * @group Etiqueta Item - End-point para la entidad Item
 * @param {string} id.path.required - Id del item que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/:id/artist', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let artistId = req.params.id;
    itemModel.getArtistForItem(artistId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar la banda a la que pertenece un item
 * @route GET /item/{id}/band
 * @group Etiqueta Item - End-point para la entidad Item
 * @param {string} id.path.required - Id del item que se esta consultando
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/:id/band', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let bandId = req.params.id;
    itemModel.getBandForItem(bandId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear una item
 * @route POST /item
 * @group Etiqueta Item - End-point para la entidad Item
 * @param {item.model} item.body.required - Item que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/item', function(req, res, next) {

    reqinfo.maininfo(req);
    itemModel.createItem(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una item
 * @route DELETE /item/{id}
 * @group Etiqueta Item - End-point para la entidad Item
 * @param {string} id.path.required - Id de la item que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Item no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/item/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let itemId = req.params.id;
        itemModel.deleteItem(itemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una item
 * @route PUT /item/{id}
 * @group Etiqueta Item - End-point para la entidad Item
 * @param {string} id.path.required - Id del item que se desea actualizar
 * @param {item.model} item.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Item no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/item/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let itemId = req.params.id;
        itemModel.updateItem(itemId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;