'use strict';
const express     = require('express');
const router      = express.Router();
const configurationModel = require(`${process.env.PATH_BASE}/models/model_configuration`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef configuration
 * @property {string} pathResources - Ruta del sistema donde están almacenados los recursos.
 * @property {string} urlResources - Url donde se tienen accesibles todos los recursos multimedia.
 * @property {string} urlApi - URL base de la API
 * @property {string} endpoints - End-points de la api
 * @property {string} rrss - Objecto JSON que contiene la info de la redes sociales.
 */

/**
 * Listar todas las configurationas
 * @route GET /configuration
 * @group Etiqueta Configuration - End-point para la entidad Configuration
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/configuration', function(req, res, next) {
    
    reqinfo.maininfo(req);
    configurationModel.listConfiguration((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una configurationa
 * @route GET /configuration/{id}
 * @group Etiqueta Configuration - End-point para la entidad Configuration
 * @param {string} id.path.required - Id del configurationa que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/configuration/:id', function(req, res, next) {
    reqinfo.maininfo(req);
    let configurationId = req.params.id;
    configurationModel.getConfiguration(configurationId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear una configurationa
 * @route POST /configuration
 * @group Etiqueta Configuration - End-point para la entidad Configuration
 * @param {configuration.model} configuration.body.required - Configurationa que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/configuration', function(req, res, next) {

    reqinfo.maininfo(req);
    configurationModel.createConfiguration(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una configurationa
 * @route DELETE /configuration/{id}
 * @group Etiqueta Configuration - End-point para la entidad Configuration
 * @param {string} id.path.required - Id de la configurationa que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Configurationa no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/configuration/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let configurationId = req.params.id;
        configurationModel.deleteConfiguration(configurationId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una configurationa
 * @route PUT /configuration/{id}
 * @group Etiqueta Configuration - End-point para la entidad Configuration
 * @param {string} id.path.required - Id del configurationa que se desea actualizar
 * @param {configuration.model} configuration.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Configurationa no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/configuration/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let configurationId = req.params.id;
        configurationModel.updateConfiguration(configurationId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
