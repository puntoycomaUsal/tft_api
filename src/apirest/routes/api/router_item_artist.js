'use strict';
const express         = require('express');
const router          = express.Router();
const itemArtistModel = require(`${process.env.PATH_BASE}/models/model_item_artist`);
const utils           = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo         = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef Item_artist
 * @property {number} artist_id - Type al que pertenece la itema
 * @property {number} item_id - Nombre del item
 * @property {number} typeitem_id - Tipo del item
 * @property {number} position - Fecha de creación de la itema
 */


/**
 * Listar todas las relaciones entre artist e item
 * @route GET /item/artist
 * @group Etiqueta Item_artist - End-point para la relación artist-item
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/artist', function(req, res, next) {

    reqinfo.maininfo(req);
    itemArtistModel.listItemArtist((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una relación artist-item
 * @route GET /item/artist/{id}
 * @group Etiqueta Item_artist - End-point para la entiendad Item
 * @param {string} id.path.required - Id de la relación
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/artist/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    let itemId = req.params.id;
    itemArtistModel.getItemArtist(itemId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Permite consultar la relación entre item y album filtrando por uno de los dos campos
 * para poder filtrar por el campo album el de item debe ser -1.
 * @route GET /item/artist/{idItem}/{idArtist}
 * @group Etiqueta Item_artist - End-point para la entidad Item
 * @param {string} idItem.path.required - Id del item que se esta consultando
 * @param {string} idAlbum.path.required - Id del album que se esta consultando
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/item/artist/:idItem/:idArtist', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let itemId = req.params['idItem'];
    let artistId = req.params['idArtist'];
    
    if (itemId !== '-1') {
        itemArtistModel.getItemArtistFromItem(itemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) res.status(200).send({type: 1, message: "OK", data: result});
        })
    }else {
        itemArtistModel.getItemArtistFromArtist(artistId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) res.status(200).send({type: 1, message: "OK", data: result});
        })
    }

});

/**
 * Crear una nueva relación artist-item
 * @route POST /item/artist
 * @group Etiqueta Item_artist - End-point para la entidad Item
 * @param {Item_artist.model} item.body.required - Item que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/item/artist', function(req, res, next) {

    reqinfo.maininfo(req);
    itemArtistModel.createItemArtist(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una itema
 * @route DELETE /item/artist/{id}
 * @group Etiqueta Item_artist - End-point para la entidad Item
 * @param {string} id.path.required - Id de la relación que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Relación no encontrada
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/item/artist/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let itemId = req.params.id;
        itemArtistModel.deleteItemArtist(itemId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una relación artist-item
 * @route PUT /item/artist/{id}
 * @group Etiqueta Item_artist - End-point para la entidad Item
 * @param {string} id.path.required - Id de la relazión que se va actualizar
 * @param {Item_artist.model} item.body.required - Datos actualizados
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Relación no encontrada
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/item/artist/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let itemId = req.params.id;
        itemArtistModel.updateItemArtist(itemId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
