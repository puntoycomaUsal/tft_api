'use strict';
const express     = require('express');
const router      = express.Router();
const typebandModel = require(`${process.env.PATH_BASE}/models/model_typeband`);
const utils       = require(`${process.env.PATH_BASE}/helpers/helper_utils`);
const reqinfo     = require(`${process.env.PATH_BASE}/helpers/helper_reqinfo`);

/**
 * @typedef typeband
 * @property {string} name - Nombre
 */


/**
 * Listar todas las typebandas
 * @route GET /typeband
 * @group Etiqueta Typeband - End-point para la entidad typeband
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/typeband', function(req, res, next) {
    
    reqinfo.maininfo(req);
    typebandModel.listTypeband((err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Consultar una typeband
 * @route GET /typeband/{id}
 * @group Etiqueta Typeband - End-point para la entidad typeband
 * @param {string} id.path.required - Id del typebanda que se desea consultar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.get('/typeband/:id', function(req, res, next) {
    
    reqinfo.maininfo(req);
    let typebandId = req.params.id;
    typebandModel.getTypeband(typebandId, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(200).send({type: 1, message: "OK", data: result});
    })

});

/**
 * Crear una typeband
 * @route POST /typeband
 * @group Etiqueta Typeband - End-point para la entidad typeband
 * @param {typeband.model} typeband.body.required - Banda que se quiere crear
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.post('/typeband', function(req, res, next) {

    reqinfo.maininfo(req);
    typebandModel.createTypeband(req.body, (err, result) => {
        if (err) res.status(500).send({type: -1, message: err});
        if (result) res.status(201).send({type: 1, message: "OK"});
    })

});

/**
 * Eliminar una typeband
 * @route DELETE /typeband/{id}
 * @group Etiqueta Typeband - End-point para la entidad typeband
 * @param {string} id.path.required - Id de la typebanda que se desea eliminar
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.delete('/typeband/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "DELETE") {
        let typebandId = req.params.id;
        typebandModel.deleteTypeband(typebandId, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                console.log(result)
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

/**
 * Actualizar una typeband
 * @route PUT /typeband/{id}
 * @group Etiqueta Typeband - End-point para la entidad typeband
 * @param {string} id.path.required - Id del typebanda que se desea actualizar
 * @param {typeband.model} typeband.body.required - Datos actualizardos
 * @returns {object} 200 - Respuesta en caso de acierto
 * @returns {object} 404 - Banda no encontrado
 * @returns {object} 500 - Error inesperado
 * @returns {Error}  default - Respuesta incorrecta
 */

router.put('/typeband/:id', function(req, res, next) {

    reqinfo.maininfo(req);
    if (req.method === "PUT") {
        let typebandId = req.params.id;
        typebandModel.updateTypeband(typebandId, req.body, (err, result) => {
            if (err) res.status(500).send({type: -1, message: err});
            if (result) {
                let status = utils.handleMysqlResult(result)
                res.status(status.code).send({type: 1, message: status.message});
            }
        })
    }

});

module.exports = router;
