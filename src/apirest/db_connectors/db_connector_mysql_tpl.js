'use strict';

/**
 * Configuración del objeto MySql necesario para realizar la
 * conexión.
 */


const mysql = require('mysql');

var connection = mysql.createConnection({
    host       : process.env.MYSQL_DB_HOST,
    user       : process.env.MYSQL_DB_USER,
    password   : process.env.MYSQL_DB_PASS,
    database   : process.env.MYSQL_DB_NAME,
    port       : process.env.MYSQL_DB_PORT,
});

exports.connection = connection;