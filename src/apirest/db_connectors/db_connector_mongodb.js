const mongoose = require('mongoose');
const winston               = require('winston');

const loggerArranque = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: `${process.env.PATH_BASE}/logs/${process.pid}.log` })
  ],
  exceptionHandlers: [
    new winston.transports.File({ filename: `${process.env.PATH_BASE}/logs/${process.pid}.log` })
  ]
});

mongoose.connect('mongodb://'+process.env.MONGO_DB_HOST+':'+process.env.MONGO_DB_PORT+'/'+process.env.MONGO_DB_NAME,{
        reconnectTries: 30,
        reconnectInterval: 1000,
        useNewUrlParser: true
  })
  .then((result) => {
    loggerArranque.info("Connection OK.")
  })
  .catch((err) => {
    loggerArranque.info(err)
  })