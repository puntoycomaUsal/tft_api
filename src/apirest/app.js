'use strict';

// Declaración de objetos necesarios para toda la app
const createError           = require('http-errors');
const express               = require('express');
const path                  = require('path');
const bodyParser            = require('body-parser')
const cookieParser          = require('cookie-parser');
const cors                  = require('cors')
const app                   = express();
const mongo_connection      = require(`${process.env.PATH_BASE}/db_connectors/db_connector_mongodb.js`);
const winston               = require('winston');

const loggerArranque = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: `${process.env.PATH_BASE}/logs/${process.pid}.log` })
  ],
  exceptionHandlers: [
    new winston.transports.File({ filename: `${process.env.PATH_BASE}/logs/${process.pid}.log` })
  ]
});

loggerArranque.info(`Worker ${process.pid} Arrancando`)

// Configuración de express
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Declaración de routers
const artistRouter              = require(`${process.env.PATH_BASE}/routes/api/router_artist`);
const bandRouter                = require(`${process.env.PATH_BASE}/routes/api/router_band`);
const typebandRouter            = require(`${process.env.PATH_BASE}/routes/api/router_typeband`);
const itemRouter                = require(`${process.env.PATH_BASE}/routes/api/router_item`);
const tagRouter                 = require(`${process.env.PATH_BASE}/routes/api/router_tag`);
const albumRouter               = require(`${process.env.PATH_BASE}/routes/api/router_album`);
const typeitemRouter            = require(`${process.env.PATH_BASE}/routes/api/router_typeitem`);
const bandArtistRouter          = require(`${process.env.PATH_BASE}/routes/api/router_band_artist`);
const bandRoleRouter            = require(`${process.env.PATH_BASE}/routes/api/router_role`);
const itemAlbumRouter           = require(`${process.env.PATH_BASE}/routes/api/router_item_album`);
const itemArtistRouter          = require(`${process.env.PATH_BASE}/routes/api/router_item_artist`);
const itemBandRouter            = require(`${process.env.PATH_BASE}/routes/api/router_item_band`);
const configurationRouter       = require(`${process.env.PATH_BASE}/routes/api/router_configuration`);
const tagAlbumRouter            = require(`${process.env.PATH_BASE}/routes/api/router_tag_album`);
const tagBandRouter             = require(`${process.env.PATH_BASE}/routes/api/router_tag_band`);
const albumCollectionRouter     = require(`${process.env.PATH_BASE}/routes/api/router_albumcollection`);
const artistCollectionRouter    = require(`${process.env.PATH_BASE}/routes/api/router_artistcollection`);
const bandCollection            = require(`${process.env.PATH_BASE}/routes/api/router_bandcollection`);
const newCollection             = require(`${process.env.PATH_BASE}/routes/api/router_newcollection`);

//Endpoints mongoDB
app.use(`/${process.env.API_VERSION}`, albumCollectionRouter);
loggerArranque.info(` Iniciando router albumcollectionRouter   .......`);
app.use(`/${process.env.API_VERSION}`, artistCollectionRouter);
loggerArranque.info(` Iniciando router artistcollectionRouter   ......`);
app.use(`/${process.env.API_VERSION}`, bandCollection);
loggerArranque.info(` Iniciando router bandcollection   ..............`);
app.use(`/${process.env.API_VERSION}`, newCollection);
loggerArranque.info(` Iniciando router newcollection   ..............`);

//Rutas relaciones
app.use(`/${process.env.API_VERSION}`, bandArtistRouter);
loggerArranque.info(` Iniciando router bandArtist   ..................`);
app.use(`/${process.env.API_VERSION}`, itemAlbumRouter);
loggerArranque.info(` Iniciando router itemAlbumRouter    ............`);
app.use(`/${process.env.API_VERSION}`, itemArtistRouter);
loggerArranque.info(` Iniciando router itemArtistRouter    ...........`);
app.use(`/${process.env.API_VERSION}`, itemBandRouter);
loggerArranque.info(` Iniciando router itemBandRouter    .............`);
app.use(`/${process.env.API_VERSION}`, tagAlbumRouter);
loggerArranque.info(` Iniciando router tagAlbumRouter    .............`);
app.use(`/${process.env.API_VERSION}`, tagBandRouter);
loggerArranque.info(` Iniciando router tagBandRouter    .............`);

//Rutas entidades simples
app.use(`/${process.env.API_VERSION}`, artistRouter);
loggerArranque.info(` Iniciando router artists   .....................`);
app.use(`/${process.env.API_VERSION}`, bandRouter);
loggerArranque.info(` Iniciando router band   ........................`);
app.use(`/${process.env.API_VERSION}`, typebandRouter);
loggerArranque.info(` Iniciando router typeband   ....................`);
app.use(`/${process.env.API_VERSION}`, itemRouter);
loggerArranque.info(` Iniciando router item   ........................`);
app.use(`/${process.env.API_VERSION}`, tagRouter);
loggerArranque.info(` Iniciando router tag   ........................`);
app.use(`/${process.env.API_VERSION}`, albumRouter);
loggerArranque.info(` Iniciando router albumRouter   .................`);
app.use(`/${process.env.API_VERSION}`, typeitemRouter);
loggerArranque.info(` Iniciando router typeitem   ....................`);
app.use(`/${process.env.API_VERSION}`, bandRoleRouter);
loggerArranque.info(` Iniciando router bandRole   ....................`);
app.use(`/${process.env.API_VERSION}`, configurationRouter);
loggerArranque.info(` Iniciando router configurationRouter   .........`);


// Declaración express-swagger-generator
const expressSwagger        = require('express-swagger-generator')(app);

// Manejador de errores, configuración.
app.use(function(err, req, res, next) {
    res.locals.message      = err.message;
    res.locals.error        = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
});

//Configuración para la documentación swagger
let options = {
    swaggerDefinition: {
        info: {
            description:    `${process.env.SWAGGER_DESCRIPTION}`,
            title:          `${process.env.SWAGGER_TITLE}`,
            version:        `${process.env.SWAGGER_VERSION}`,
        },
        host:       `${process.env.HOST}:${process.env.PORT}`,
        basePath:   `${process.env.SWAGGER_BASEPATH}`,
        produces:   ['application/json'],
        schemes:    ['http'],
    },
    basedir: __dirname, 
    files: [`${process.env.PATH_BASE}/routes/**/*.js`]
};
expressSwagger(options)
loggerArranque.info(` Documentación Api Swagger http://${process.env.HOST}:${process.env.PORT}/api-docs`);

module.exports = app;
